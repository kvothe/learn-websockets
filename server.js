var http = require('http');
var WebSocket = require('ws');
var express = require('express');

var app = express();
var server = http.createServer(app);

var wss = new WebSocket.Server({server: server});

app.get('/', function(req, res) {
    res.sendFile('index.html', {root: __dirname});
})

app.use(express.static('public'));

var clients = {};
var messages = [];

wss.on('connection', function connection(ws) {
    var id = Math.random();
    clients[id] = ws;
    console.log("new connection " + id);
    var messages_len = messages.length;
    for (var msg = 0; msg < messages_len; msg++ ) {
        ws.send(messages[msg]);
    }
    console.log("messages was sent");
    console.log(messages);


    ws.on('message', function(message) {
        console.log("received message " + message);
        for (var key in clients) {
            clients[key].send(message)
            console.log("message is: " + message);
            messages.push(message);
        }
    });

    ws.on('close', function(ws) {
        console.log('connection closed ' + id);
        delete clients[id];
    });
});


server.listen(8084, function() {
    console.log('Listening on: ' + server.address().port);
});
