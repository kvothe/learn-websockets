'use strict';

var myapp = angular.module('WebSocketApp', ['ngWebsocket']);

myapp.controller('WebSocketAppCtrl', ['$scope', '$websocket',
 	function($scope, $websocket){
 		function $(a) {
			return document.getElementById(a)
		}

		var ws = $websocket.$new('ws://localhost:8084');

		ws.$on('$open', function() {
			console.log('websocket is open');
		});

		ws.$on('$close', function() {
			console.log('websocket was closed');
		});

		ws.$on('$message', function(event) {
			var inmessage = event.data;
			showMessage(inmessage);
		});

		function showMessage(message) {
			var messageElem = document.createElement('div');
			messageElem.appendChild(document.createTextNode(message));
			$("output").appendChild(messageElem);
		}

		$scope.sendMessage = function() {
			var outmessage = $scope.data;
			ws.$emit('message', outmessage);
		}
}]);
